package com.damian.mycomposer.helpers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
public class ImageUploadHelperTests {
    @Test
    public void shouldGenerateRandomFileNameForDifferentFileNames() {
        String fileName1 = UploadFilesHelper.generateRandomFileName("fileName1");
        String fileName2 = UploadFilesHelper.generateRandomFileName("fileName2");
        assertNotEquals(fileName1, fileName2);
    }

    @Test
    public void shouldGenerateRandomFileNameForTheSameFileNames() {
        String fileName1 = UploadFilesHelper.generateRandomFileName("fileName1");
        String fileName2 = UploadFilesHelper.generateRandomFileName("fileName1");
        assertNotEquals(fileName1, fileName2);
    }
}
