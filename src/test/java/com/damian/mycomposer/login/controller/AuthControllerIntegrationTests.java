package com.damian.mycomposer.login.controller;

import com.damian.mycomposer.login.message.request.LoginForm;
import com.damian.mycomposer.login.message.request.SignUpForm;
import com.damian.mycomposer.login.model.User;
import com.damian.mycomposer.login.repository.RoleRepository;
import com.damian.mycomposer.login.repository.UserRepository;
import com.damian.mycomposer.login.security.jwt.JwtProviderIntegrationTests;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthControllerIntegrationTests {
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    private AuthenticationManager authenticationManager;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    private PasswordEncoder encoder;
    private JwtProviderIntegrationTests jwtProvider;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void signInWithExistingUser_shouldSucceedWith200() throws Exception{
        LoginForm loginForm = new LoginForm();
        loginForm.setUsername("user");
        loginForm.setPassword("123456");

        mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginForm)))
                .andExpect(status().isOk());
    }

    @Test
    public void signInWithNonExistingUser_shouldFailWith401() throws Exception{
        LoginForm loginForm = new LoginForm();
        loginForm.setUsername("username");
        loginForm.setPassword("password");

        mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginForm)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void correctSignUpWith_shouldSucceedWith200() throws Exception {
        String username = UUID.randomUUID().toString().replace("-","");
        SignUpForm signUpForm = new SignUpForm();
        signUpForm.setName(username);
        signUpForm.setUsername(username);
        signUpForm.setEmail(username+"@gmail.com");
        signUpForm.setRole(new HashSet<String>(Arrays.asList("user")));
        signUpForm.setPassword("123456");

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signUpForm)))
                .andExpect(status().isOk());

        Optional<User> user = userRepository.findByUsername(username);
        assertEquals(username, user.get().getUsername());

        String result = resultActions.andReturn().getResponse().getContentAsString();
        assertEquals("{\"message\":\"User registered successfully!\"}", result);
    }

    @Test
    public void signUpWithExistingUserName_shouldFailWith400() throws Exception{
        SignUpForm signUpForm = new SignUpForm();
        signUpForm.setName("user");
        signUpForm.setUsername("user");
        signUpForm.setEmail("user123@gmail.com");
        signUpForm.setRole(new HashSet<String>(Arrays.asList("user")));
        signUpForm.setPassword("123456");

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signUpForm)))
                .andExpect(status().isConflict());

        String result = resultActions.andReturn().getResponse().getContentAsString();
        assertEquals("{\"message\":\"Fail -> Username is already taken!\"}", result);
    }

    @Test
    public void signUpWithExistingEmail_shouldFailWith400() throws Exception{
        SignUpForm signUpForm = new SignUpForm();
        signUpForm.setName("user123");
        signUpForm.setUsername("user123");
        signUpForm.setEmail("user@gmail.com");
        signUpForm.setRole(new HashSet<String>(Arrays.asList("user")));
        signUpForm.setPassword("123456");

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signUpForm)))
                .andExpect(status().isConflict()).andDo(MockMvcResultHandlers.print());

        String result = resultActions.andReturn().getResponse().getContentAsString();
        assertEquals("{\"message\":\"Fail -> Email is already taken!\"}", result);
    }
}
