package com.damian.mycomposer.login.controller;

import com.damian.mycomposer.login.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerIntegrationTests {
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @WithMockUser(username = "user", password = "123456")
    @Test
    public void returnUserInfoForAuthorizedUser_shouldSucceedWith200() throws Exception {
        String expectedResponse = "{" +
                "\"name\":\"user\"," +
                "\"email\":\"user@gmail.com\"," +
                "\"imageName\":\"120220-023628_2_dziad2.png\"," +
                "\"username\":\"user\"" +
                "}";
        mockMvc.perform(get("/api/user")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(expectedResponse));
    }

    @Test
    public void returnUserInfoForUnauthorizedUser_shouldFailWith401() throws Exception {
        mockMvc.perform(get("/api/user")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @WithMockUser(username = "user", password = "123456")
    @Test
    public void returnUserImageForAuthorizedUser_shouldSucceedWith200() throws Exception {
        mockMvc.perform(get("/api/user/image")
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isOk());
    }

    @Test
    public void returnUserImageForUnAuthorizedUser_shouldFailWith401() throws Exception {
        mockMvc.perform(get("/api/user/image")
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(status().isUnauthorized());
    }
}
