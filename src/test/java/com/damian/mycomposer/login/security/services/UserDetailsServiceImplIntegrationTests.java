package com.damian.mycomposer.login.security.services;

import com.damian.mycomposer.login.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDetailsServiceImplIntegrationTests {
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @WithMockUser(username = "user", password = "123456")
    @Test
    public void loadUserByUserName() throws UsernameNotFoundException {
        UserDetails userDetails = userDetailsService.loadUserByUsername("user");
        assertEquals("user", userDetails.getUsername());
    }
}
