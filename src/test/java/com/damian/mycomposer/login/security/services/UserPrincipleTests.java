package com.damian.mycomposer.login.security.services;

import com.damian.mycomposer.login.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class UserPrincipleTests {
    @Test
    public void shouldReturnUserPrincipleForGivenUser() {
        User user = new User("user", "user", "user@gmail.com", "123456", "imageFileName");
        UserPrinciple userPrinciple = UserPrinciple.build(user);
        assertEquals(user.getUsername(), UserPrinciple.build(user).getUsername());
        assertEquals(user.getEmail(), UserPrinciple.build(user).getEmail());
    }
}
