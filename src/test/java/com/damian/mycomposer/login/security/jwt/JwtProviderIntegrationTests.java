package com.damian.mycomposer.login.security.jwt;

import com.damian.mycomposer.login.message.request.SignUpForm;
import com.damian.mycomposer.login.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JwtProviderIntegrationTests {
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    private StringBuilder username = new StringBuilder();
    private StringBuilder username2 = new StringBuilder();

    @Before
    public void setUp() throws Exception{
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .apply(springSecurity())
                .build();

        username.append(UUID.randomUUID().toString().replace("-",""));
        SignUpForm signUpForm = new SignUpForm();
        signUpForm.setName(username.toString());
        signUpForm.setUsername(username.toString());
        signUpForm.setEmail(username+"@gmail.com");
        signUpForm.setRole(new HashSet<String>(Arrays.asList("user")));
        signUpForm.setPassword("123456");

        username2.append(UUID.randomUUID().toString().replace("-",""));
        SignUpForm signUpForm2 = new SignUpForm();
        signUpForm2.setName(username2.toString());
        signUpForm2.setUsername(username2.toString());
        signUpForm2.setEmail(username2+"@gmail.com");
        signUpForm2.setRole(new HashSet<String>(Arrays.asList("user")));
        signUpForm2.setPassword("123456");

        mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signUpForm)))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signUpForm2)))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldGenerateDifferentJwtTokensForDifferentUsers() {
        Authentication authenticationForFirstUser = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username,"123456"));
        String tokenForFirstUser = jwtProvider.generateJwtToken(authenticationForFirstUser);

        Authentication authenticationForSecondUser = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username2,"123456"));
        String tokenForFirstSecond = jwtProvider.generateJwtToken(authenticationForSecondUser);

        assertNotEquals(tokenForFirstUser, tokenForFirstSecond);
    }

    @Test
    public void shouldGetUserNameForGivenJwtTokens() {
        Authentication authenticationForFirstUser = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username,"123456"));
        String tokenForFirstUser = jwtProvider.generateJwtToken(authenticationForFirstUser);

        assertEquals(username.toString(), jwtProvider.getUserNameFromJwtToken(tokenForFirstUser));
    }
}
