package com.damian.mycomposer.login.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTests {
    @Autowired
    private UserService userService;

    @WithMockUser(username = "user", password = "123456")
    @Test
    public void returnCurrentlyLoggedUser_shouldReturnCurrentlyLoggedUser() {
        assertEquals("user", userService.getCurrentlyLoggedUser().get().getUsername());
    }
}
