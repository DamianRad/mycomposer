package com.damian.mycomposer.service;

import com.damian.mycomposer.helpers.Constants;
import com.damian.mycomposer.helpers.UploadFilesHelper;
import com.damian.mycomposer.login.model.User;
import com.damian.mycomposer.model.Album;
import com.damian.mycomposer.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class AlbumService {
    @Autowired
    private AlbumRepository albumRepository;

    public List<Album> findAllAlbumsForUser(User user) {
        return albumRepository.findAllByUserId(user.getId());
    }

    public Album saveAlbum(Album albumRequest, MultipartFile albumImage, Long userId) {
        if (albumImage != null) {
            String imageFileName = UploadFilesHelper.generateRandomFileName(albumImage.getOriginalFilename());
            UploadFilesHelper.saveFile(albumImage, imageFileName,  Constants.IMAGE_FILE);
            albumRequest.setImagePath(imageFileName);
        }
        albumRequest.setUserId(userId);
        return albumRepository.save(albumRequest);
    }

    public void deleteAlbum(Long id) {
        albumRepository.deleteById(id);
    }

    public Album updateAlbum(Album albumRequest, Long albumId, MultipartFile albumImage) {
        if (albumImage != null) {
            String imageFileName = UploadFilesHelper.generateRandomFileName(albumImage.getOriginalFilename());
            UploadFilesHelper.saveFile(albumImage, imageFileName, Constants.IMAGE_FILE);
            albumRequest.setImagePath(imageFileName);
        }
        return albumRepository.findById(albumId).map(album -> {
            if(albumRequest.getImagePath() == null && album.getImagePath() != null) {
                UploadFilesHelper.deleteFile(album.getImagePath(),  Constants.IMAGE_FILE);
            }
            album.setTitle(albumRequest.getTitle());
            album.setDescription(albumRequest.getDescription());
            album.setImagePath(albumRequest.getImagePath());
            return albumRepository.save(album);
        }).orElseThrow(() -> new EntityNotFoundException("Album with id: " + albumId + " not found."));
    }

    public String getImageFileName(Long albumId) {
        return this.albumRepository.findById(albumId).get().getImagePath();
    }
}
