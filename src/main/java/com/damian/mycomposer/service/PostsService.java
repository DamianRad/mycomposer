package com.damian.mycomposer.service;

import com.damian.mycomposer.login.model.User;
import com.damian.mycomposer.model.Comment;
import com.damian.mycomposer.model.LikedPost;
import com.damian.mycomposer.model.Post;
import com.damian.mycomposer.repository.CommentsRepository;
import com.damian.mycomposer.repository.LikedPostRepository;
import com.damian.mycomposer.repository.PostsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PostsService {

    @Autowired
    private PostsRepository postsRepository;
    @Autowired
    private CommentsRepository commentsRepository;
    @Autowired
    private LikedPostRepository likedPostRepository;

    public ResponseEntity<?> addPost(Post post, User user) {
        post.setUsername(user.getUsername());
        post.setUserId(user.getId());
        post.setPostDate(new Date());
        this.postsRepository.save(post);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<?> addComment(Comment comment, User user, Long postId) {
        comment.setUsername(user.getUsername());
        comment.setUserId(user.getId());
        comment.setPost(postsRepository.findById(postId).get());
        comment.setCommentDate(new Date());
        this.commentsRepository.save(comment);
        return ResponseEntity.ok().build();
    }

    @Transactional
    public Long likePost(LikedPost likedPost) {
        Optional<LikedPost> existingLikedPost = this.likedPostRepository.findByPostIdAndUserId(likedPost.getPostId(), likedPost.getUserId());
        if(existingLikedPost.isEmpty()) {
            this.likedPostRepository.save(likedPost);
        } else {
            this.likedPostRepository.deleteByPostIdAndUserId(likedPost.getPostId(), likedPost.getUserId());
        }
        return this.likedPostRepository.countByPostId(likedPost.getPostId());
    }

    public List<Post> getPosts() {
        List<Post> posts = this.postsRepository.findAll();
        for(Post post : posts) {
            post.setTotalLikes(this.likedPostRepository.countByPostId(post.getId()));
        }
        return posts;
    }

    public List<Comment> getComments(Long postId) {
        return this.commentsRepository.findAllByPostId(postId);
    }

    @Transactional
    public ResponseEntity deletePost(Long postId, Long currentLoggedUserId) {
        Post post = this.postsRepository.findById(postId).get();
        if(post.getUserId().equals(currentLoggedUserId)) {
            this.likedPostRepository.deleteByPostId(post.getId());
            this.postsRepository.delete(post);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("You can delete only your posts.");
        }
    }
}
