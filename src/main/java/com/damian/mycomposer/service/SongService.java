package com.damian.mycomposer.service;

import com.damian.mycomposer.helpers.Constants;
import com.damian.mycomposer.helpers.UploadFilesHelper;
import com.damian.mycomposer.model.Song;
import com.damian.mycomposer.repository.AlbumRepository;
import com.damian.mycomposer.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

@Service
public class SongService {
    @Autowired
    private SongRepository songRepository;

    @Autowired
    private AlbumRepository albumRepository;

    public List<Song> findAllByAlbumId(Long albumId) {
        return songRepository.findAllByAlbumId(albumId);
    }

    public Song saveSong(Song songRequest, MultipartFile songMusicFile, Long albumId) {
        if (songMusicFile != null) {
            String imageFileName = UploadFilesHelper.generateRandomFileName(songMusicFile.getOriginalFilename());
            UploadFilesHelper.saveFile(songMusicFile, imageFileName, Constants.MUSIC_FILE);
            songRequest.setMusicFileName(imageFileName);
        }
        return albumRepository.findById(albumId).map(album -> {
            songRequest.setAlbum(album);
            return songRepository.save(songRequest);
        }).orElseThrow(() -> new EntityNotFoundException("Album id: " + albumId + " not found"));
    }

    public Song updateSong(Long albumId, Long songId, Song songRequest, MultipartFile songMusicFile) {
        if(!albumRepository.existsById(albumId)) {
            throw new EntityNotFoundException("Album with id: " + albumId + " not found");
        }

        if (songMusicFile != null) {
            String imageFileName = UploadFilesHelper.generateRandomFileName(songMusicFile.getOriginalFilename());
            UploadFilesHelper.saveFile(songMusicFile, imageFileName, Constants.MUSIC_FILE);
            songRequest.setMusicFileName(imageFileName);
        }

        return songRepository.findById(songId).map(song -> {
            song.setText(songRequest.getText());
            song.setTitle(songRequest.getTitle());
            song.setAuthor(songRequest.getAuthor());
            song.setMusicFileName(songRequest.getMusicFileName());
            return songRepository.save(song);
        }).orElseThrow(() -> new EntityNotFoundException("Song with id: " + songId + " not found."));
    }

    public ResponseEntity<?> deleteSong(Long albumId, Long songId) {
        return songRepository.findByAlbumIdAndId(albumId, songId).map(song -> {
            if(song.getMusicFileName() != null) {
                UploadFilesHelper.deleteFile(song.getMusicFileName(), Constants.MUSIC_FILE);
            }
            songRepository.delete(song);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new EntityNotFoundException("Song with songId: " + songId + " and albumId id : " + albumId + " not found."));
    }

    public ResponseEntity getMusicFileForSong(Long albumId, Long songId) throws FileNotFoundException {
        String musicFileName = this.songRepository.findByAlbumIdAndId(albumId, songId).get().getMusicFileName();
        String filePath = System.getProperty("user.dir").replaceAll("\\\\", "/") + "/music/" + musicFileName;
        long length = new File(filePath).length();

        InputStreamResource inputStreamResource = new InputStreamResource( new FileInputStream(filePath));
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentLength(length);
        httpHeaders.setCacheControl(CacheControl.noCache().getHeaderValue());
        return new ResponseEntity(inputStreamResource, httpHeaders, HttpStatus.OK);
    }

}
