package com.damian.mycomposer;

import com.damian.mycomposer.helpers.Constants;
import com.damian.mycomposer.helpers.UploadFilesHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

@SpringBootApplication
public class MycomposerApplication {
	public static void main(String[] args) {
	    new File(UploadFilesHelper.uploadDir + Constants.IMAGE_FILE).mkdir();
	    new File(UploadFilesHelper.uploadDir + Constants.MUSIC_FILE).mkdir();
		SpringApplication.run(MycomposerApplication.class, args);
	}
}
