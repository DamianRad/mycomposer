package com.damian.mycomposer.repository;

import com.damian.mycomposer.login.model.User;
import com.damian.mycomposer.model.Album;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlbumRepository extends JpaRepository<Album, Long> {
    List<Album> findAllByUserId(Long userId);
}
