package com.damian.mycomposer.repository;

import com.damian.mycomposer.model.Song;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SongRepository extends JpaRepository<Song, Long> {
    List<Song> findAllByAlbumId(Long albumId);
    Optional<Song> findByAlbumIdAndId(Long albumId, Long id);
}
