package com.damian.mycomposer.repository;

import com.damian.mycomposer.model.LikedPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LikedPostRepository extends JpaRepository<LikedPost, Long> {
    Optional<LikedPost> findByPostIdAndUserId(Long postId, Long userId);
    Long countByPostId(Long postId);
    void deleteByPostIdAndUserId(Long postId, Long userId);
    void deleteByPostId(Long postId);
}
