package com.damian.mycomposer.controller;

import com.damian.mycomposer.model.Song;
import com.damian.mycomposer.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class SongController {
    private SongService songService;

    @Autowired
    public SongController(SongService songService) {
        this.songService = songService;
    }

    @GetMapping("/api/albums/{albumId}/songs")
    public List<Song> getSongs(@PathVariable Long albumId) {
        return songService.findAllByAlbumId(albumId);
    }

    @PostMapping(value = "/api/albums/{albumId}/songs", consumes = {"multipart/form-data"})
    public Song createSong(@PathVariable(value = "albumId") Long albumId,
                           @RequestPart("songData") Song songData,
                           @RequestPart(value = "songMusicFile", required = false) MultipartFile songMusicFile) {
        return songService.saveSong(songData, songMusicFile, albumId);
    }

    @PutMapping("/api/albums/{albumId}/songs/{songId}")
    public Song updateSong(@PathVariable Long albumId,
                           @PathVariable Long songId,
                           @RequestPart("songData") Song songData,
                           @RequestPart(value = "songMusicFile", required = false) MultipartFile songMusicFile) {
        return songService.updateSong(albumId, songId, songData, songMusicFile);
    }

    @DeleteMapping("/api/albums/{albumId}/songs/{songId}")
    public ResponseEntity<?> deleteSong(@PathVariable Long albumId,
                                        @PathVariable Long songId) {
        return songService.deleteSong(albumId, songId);
    }

    @GetMapping(value = "/api/albums/{albumId}/songs/{songId}/music", produces = "audio/mp3")
    public ResponseEntity getMusicFileForSong(@PathVariable Long albumId,
                                              @PathVariable Long songId) throws FileNotFoundException {
        return this.songService.getMusicFileForSong(albumId, songId);
    }
}
