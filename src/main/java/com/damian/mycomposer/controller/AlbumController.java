package com.damian.mycomposer.controller;

import com.damian.mycomposer.helpers.UploadFilesHelper;
import com.damian.mycomposer.login.service.UserService;
import com.damian.mycomposer.model.Album;
import com.damian.mycomposer.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class AlbumController {
    private AlbumService albumService;
    private UserService userService;

    @Autowired
    public AlbumController(AlbumService albumService, UserService userService) {
       this.albumService = albumService;
       this.userService = userService;
    }

    @GetMapping("/api/albums")
    public List<Album> getAlbums() {
        return this.albumService.findAllAlbumsForUser(this.userService.getCurrentlyLoggedUser().get());
    }

    @GetMapping(value = "/api/albums/{albumId}/image")
    public @ResponseBody byte[] getImage(@PathVariable Long albumId) throws IOException {
        String imageFileName = albumService.getImageFileName(albumId);
        return UploadFilesHelper.extractBytes(imageFileName);
    }

    @PostMapping(path = "/api/albums", consumes = {"multipart/form-data"})
    public Album createAlbum(@RequestPart("albumData") @Valid Album albumData,
                             @RequestPart(name = "albumImage", required = false) MultipartFile albumImage) {
        return this.albumService.saveAlbum(albumData, albumImage, this.userService.getCurrentlyLoggedUser().get().getId());
    }

    @PutMapping(path = "/api/albums/{albumId}")
    public Album updateAlbum(@PathVariable Long albumId,
                             @RequestPart("albumData") @Valid Album albumData,
                             @RequestPart(value = "albumImage", required = false) MultipartFile albumImage) {
        return albumService.updateAlbum(albumData, albumId, albumImage);
    }

    @DeleteMapping(path = "/api/albums/{albumId}")
    public void deleteAlbum(@PathVariable Long albumId) {
        albumService.deleteAlbum(albumId);
    }
}
