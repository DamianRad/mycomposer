package com.damian.mycomposer.controller;

import com.damian.mycomposer.login.service.UserService;
import com.damian.mycomposer.model.Comment;
import com.damian.mycomposer.model.LikedPost;
import com.damian.mycomposer.model.Post;
import com.damian.mycomposer.service.PostsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class PostsController {
    private PostsService postsService;
    private UserService userService;

    @Autowired
    public PostsController(PostsService postsService, UserService userService) {
        this.postsService = postsService;
        this.userService = userService;
    }

    @PostMapping("/api/posts")
    public ResponseEntity addPost(@RequestBody Post post) {
        return this.postsService.addPost(post, this.userService.getCurrentlyLoggedUser().get());
    }

    @GetMapping("/api/posts")
    public List<Post> getPosts() {
        return this.postsService.getPosts();
    }

    @GetMapping("/api/posts/{postId}/comment")
    public List<Comment> getComments(@PathVariable Long postId) {
        return this.postsService.getComments(postId);
    }

    @PostMapping("/api/posts/{postId}/comment")
    public ResponseEntity postComment(@RequestBody Comment comment, @PathVariable Long postId) {
        return this.postsService.addComment(comment, this.userService.getCurrentlyLoggedUser().get(), postId);
    }

    @PostMapping("/api/posts/like")
    public Long likePost(@RequestBody LikedPost likedPost) {
        likedPost.setUserId(this.userService.getCurrentlyLoggedUser().get().getId());
        return this.postsService.likePost(likedPost);
    }

    @DeleteMapping("/api/posts/{postId}")
    public void deletePost(@PathVariable Long postId) {
        this.postsService.deletePost(postId, this.userService.getCurrentlyLoggedUser().get().getId());
    }
}
