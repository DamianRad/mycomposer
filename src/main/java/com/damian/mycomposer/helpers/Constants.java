package com.damian.mycomposer.helpers;

public class Constants {
    public static final String MUSIC_FILE = "/music";
    public static final String IMAGE_FILE = "/image";
}
