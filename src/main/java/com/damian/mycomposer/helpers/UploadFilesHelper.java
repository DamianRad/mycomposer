package com.damian.mycomposer.helpers;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class UploadFilesHelper {
    public static String uploadDir = System.getProperty("user.dir");

    public static void saveFile(MultipartFile multipartFile, String fileName, String fileType){
        Path fileNameAndPath = Paths.get(UploadFilesHelper.uploadDir + fileType, fileName);
        try {
            Files.write(fileNameAndPath, multipartFile.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteFile(String fileName, String fileType) {
        Path fileNameAndPath = Paths.get(UploadFilesHelper.uploadDir + fileType, fileName);
        try {
            Files.delete(fileNameAndPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String generateRandomFileName(String originalFileName) {
        Random random = new Random();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy-hhmmssSSS");
        return String.format("%s_%s_%s", sdf.format(new Date()), random.nextInt(1000), originalFileName);
    }

    public static byte[] extractBytes(String imageName) throws IOException {
        String path = System.getProperty("user.dir").replaceAll("\\\\", "/") + "/images/" + imageName;
        File imgPath = new File(path);
        return Files.readAllBytes(imgPath.toPath());
    }
}
