package com.damian.mycomposer.login.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
