package com.damian.mycomposer.login.model;

public class UserInfo {
    private Long id;
    private String name;
    private String email;
    private String imageName;
    private String username;

    public UserInfo(Long id, String name, String email, String imageName, String username) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.imageName = imageName;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }
}
