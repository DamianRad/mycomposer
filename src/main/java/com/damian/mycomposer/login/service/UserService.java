package com.damian.mycomposer.login.service;

import com.damian.mycomposer.helpers.Constants;
import com.damian.mycomposer.helpers.UploadFilesHelper;
import com.damian.mycomposer.login.message.response.ResponseMessage;
import com.damian.mycomposer.login.model.User;
import com.damian.mycomposer.login.model.UserInfo;
import com.damian.mycomposer.login.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public Optional<User> getCurrentlyLoggedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        return userRepository.findByUsername(username);
    }

    public ResponseEntity<?> uploadUserImage(String userName, MultipartFile userImage) {
        StringBuilder imageFileName = new StringBuilder();
        if (userImage != null) {
            imageFileName.append(UploadFilesHelper.generateRandomFileName(userImage.getOriginalFilename()));
            UploadFilesHelper.saveFile(userImage, imageFileName.toString(), Constants.IMAGE_FILE);
        }

        return userRepository.findByUsername(userName).map(
                user -> {
                    user.setImageName(imageFileName.toString());
                    userRepository.save(user);
                    return ResponseEntity.ok().build();
                }
        ).orElseThrow(() -> new EntityNotFoundException("Cannot upload image not found."));
    }

    public ResponseEntity<?> updateUser(UserInfo userInfo, String userName) {
        if (userRepository.existsByEmail(userInfo.getEmail())) {
            return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already taken!"), HttpStatus.BAD_REQUEST);
        }

        return userRepository.findByUsername(userName).map(
                user -> {
                    user.setName(userInfo.getName());
                    user.setEmail(userInfo.getEmail());
                    userRepository.save(user);
                    return ResponseEntity.ok().build();
                }
        ).orElseThrow(() -> new EntityNotFoundException("Cannot update user."));
    }

}
