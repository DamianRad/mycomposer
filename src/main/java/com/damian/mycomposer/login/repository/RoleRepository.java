package com.damian.mycomposer.login.repository;

import com.damian.mycomposer.login.model.Role;
import com.damian.mycomposer.login.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
