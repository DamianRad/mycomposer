package com.damian.mycomposer.login.controller;

import com.damian.mycomposer.helpers.UploadFilesHelper;
import com.damian.mycomposer.login.model.User;
import com.damian.mycomposer.login.model.UserInfo;
import com.damian.mycomposer.login.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/api/user")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public UserInfo userAccess() {
        Optional<User> currentlyLoggedUser = this.userService.getCurrentlyLoggedUser();
        return new UserInfo(
                currentlyLoggedUser.get().getId(),
                currentlyLoggedUser.get().getName(),
                currentlyLoggedUser.get().getEmail(),
                currentlyLoggedUser.get().getImageName(),
                currentlyLoggedUser.get().getUsername()
        );
    }

    @GetMapping("/api/user/image")
    public @ResponseBody byte[] getImage() throws IOException {
        return UploadFilesHelper.extractBytes(this.userService.getCurrentlyLoggedUser().get().getImageName());
    }

    @PostMapping("/api/user/image")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> uploadUserImage(@RequestPart("userImage") MultipartFile userImage) {
        return userService.uploadUserImage(this.userService.getCurrentlyLoggedUser().get().getUsername(), userImage);
    }

    @PutMapping("/api/user")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> updateUser(@RequestBody UserInfo userInfo) {
        return userService.updateUser(userInfo, this.userService.getCurrentlyLoggedUser().get().getUsername());
    }
}
